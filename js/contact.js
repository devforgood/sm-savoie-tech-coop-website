new Vue({
    el: '.app',
    data() {
        return {
            form: {
                name: '',
                email: '',
                message: '',
                phone: ''
            }
        }
    },
    methods: {
        async send() {
            if (!this.form.email) return alert('Email est requis')
            if (!this.form.message) return alert('Message est requis')
            try {
                murlytics.track({ name: 'CONTACT_FORM', form: this.form })
                await api.funql({
                    namespace: 'savoietc',
                    name: 'contactForm',
                    args: [
                        Object.assign({}, this.form, {
                            creation_date: Date.now()
                        })
                    ]
                })
                this.form = {}
                alert(`Fort et clair, merci !. Nous vous contacterons bientôt.`)
            } catch (err) {
                alert(
                    `Ups... Quelque chose a cassé, pouvez-vous nous contacter par facebook?`
                )
            }
        }
    },
    mounted() { }
})