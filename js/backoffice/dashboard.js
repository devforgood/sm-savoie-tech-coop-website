import {
    default as stylesMixin,
    template as styleMixinTmpl
} from '../mixins/styles'

export default {
    mixins: [stylesMixin],
    name: 'dashboard',
    props: [],
    template: styleMixinTmpl(`
    <div ref="scope">
        <div class="dashboard" ref="root">
            <h2>Tableau de bord</h2>
        </div>
    </div>
    `),
    data() {
        var self = this
        return {
            styles: `
            .dashboard{
                padding:5px;
            }
            
            @media only screen and (max-width: 639px) {
                
            }
        `
        }
    },
    computed: {},
    methods: {},
    mounted() {}
}